<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Account;
use App\Models\OutcomeOperation;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

final class OutcomeOperationFactory extends Factory
{
    protected $model = OutcomeOperation::class;

    public function definition(): array
    {
        return [
            'title' => $this->faker->word,
            'date'  => $this->faker->dateTimeThisYear,
            'sum'   => $this->faker->randomNumber(5),
            'note'  => $this->faker->word,

            'category_id' => random_int(1,12),
            'account_id'  => function () {
                return Account::factory()->create()->id;
            },
        ];
    }
}

<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\OutcomeCategory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

final class OutcomeCategoryFactory extends Factory
{
    protected $model = OutcomeCategory::class;

    public function definition(): array
    {
        return [
            'title'      => $this->faker->word,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}

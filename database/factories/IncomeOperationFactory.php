<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Account;
use App\Models\IncomeOperation;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

final class IncomeOperationFactory extends Factory
{
    protected $model = IncomeOperation::class;

    public function definition(): array
    {
        return [
            'title' => $this->faker->word,
            'date'  => $this->faker->dateTimeThisYear,
            'sum'   => $this->faker->randomNumber(5),
            'note'  => $this->faker->word,

            'category_id' => random_int(1,3),
            'account_id'  => function () {
                return Account::factory()->create()->id;
            },
        ];
    }
}

<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call(AccountSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(IncomeOperationSeeder::class);
        $this->call(OutcomeOperationSeeder::class);
    }
}

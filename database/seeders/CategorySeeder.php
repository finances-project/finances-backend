<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\IncomeCategory;
use App\Models\OutcomeCategory;
use Illuminate\Database\Seeder;

final class CategorySeeder extends Seeder
{
    public function run(): void
    {
        IncomeCategory::factory()->createMany([
            ['title' => 'Зарплата'],
            ['title' => 'Премия'],
            ['title' => 'Возврат долга'],
        ]);
        OutcomeCategory::factory()->createMany([
            ['title' => 'Доставки и рестораны'],
            ['title' => 'Продукты'],
            ['title' => 'Транспорт - автобус'],
            ['title' => 'Транспорт - такси'],
            ['title' => 'Транспорт - другое'],
            ['title' => 'Покупки игр'],
            ['title' => 'Покупки софта'],
            ['title' => 'Подписки на онлайн сервисы'],
            ['title' => 'Коммунальные платежи'],
            ['title' => 'Одежда и обувь'],
            ['title' => 'Предметы обихода'],
            ['title' => 'Ремонт'],
        ]);
    }
}

<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\IncomeOperation;
use Illuminate\Database\Seeder;

final class IncomeOperationSeeder extends Seeder
{
    public function run(): void
    {
        IncomeOperation::factory()->times(1000)->create([
            'account_id' => 1,
        ])->each(function (IncomeOperation $operation) {
            $operation->createOperationRegisters();
        });
    }
}

<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\OutcomeOperation;
use Illuminate\Database\Seeder;

final class OutcomeOperationSeeder extends Seeder
{
    public function run(): void
    {
        OutcomeOperation::factory()->times(1000)->create([
            'account_id' => 1,
        ])->each(function (OutcomeOperation $operation) {
            $operation->createOperationRegisters();
        });
    }
}

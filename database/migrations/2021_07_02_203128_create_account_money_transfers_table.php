<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class CreateAccountMoneyTransfersTable extends Migration
{
    public function up(): void
    {
        Schema::create('account_money_transfers', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->foreignId('account_in_id')->references('id')->on('accounts')->cascadeOnDelete();
            $table->foreignId('account_out_id')->references('id')->on('accounts')->cascadeOnDelete();
            $table->unsignedBigInteger('sum');
            $table->string('note')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('account_money_transfers');
    }
}
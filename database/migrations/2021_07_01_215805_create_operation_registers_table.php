<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class CreateOperationRegistersTable extends Migration
{
    public function up(): void
    {
        Schema::create('operation_registers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->morphs('operation');
            $table->foreignId('account_id')->constrained();
            $table->bigInteger('sum');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('operation_registers');
    }
}
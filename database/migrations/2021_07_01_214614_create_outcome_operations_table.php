<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class CreateOutcomeOperationsTable extends Migration
{
    public function up(): void
    {
        Schema::create('outcome_operations', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->date('date');
            $table->foreignId('category_id')->references('id')->on('outcome_categories');
            $table->foreignId('account_id')->constrained();
            $table->bigInteger('sum');
            $table->string('note')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('outcome_operations');
    }
}
docker = docker-compose

# Запустить Docker демона
.PHONY: run
run:
	${docker} up -d

install:
	${docker} up -d
	docker exec php-fpm-finances composer i
	docker exec php-fpm-finances php artisan key:generate

# Остановить работу Docker'а
.PHONY: stop
stop:
	${docker} stop

# Зайти в bash php-fpm
.PHONY: php
php:
	${docker} exec php-fpm bash -l

# Зайти в sh postgres
.PHONY: postgres
postgres:
	${docker} exec postgres sh -l

# Зайти в sh nginx
.PHONY: nginx
nginx:
	${docker} exec nginx sh -l
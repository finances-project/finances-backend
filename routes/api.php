<?php

declare(strict_types=1);

use App\Http\Controllers\AccountController;
use App\Http\Controllers\Categories\OutcomeCategoriesController;
use App\Http\Controllers\Operations\AccountMoneyTransferController;
use App\Http\Controllers\Operations\ActualizationController;
use App\Http\Controllers\Operations\IncomeOperationController;
use App\Http\Controllers\Operations\OutcomeOperationController;
use App\Http\Controllers\Statistics\BalanceController;
use App\Http\Controllers\Statistics\IncomeCategoriesReportController;
use App\Http\Controllers\Statistics\OutcomeCategoriesReportController;
use App\Http\Controllers\Statistics\SnapshotReportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {
    Route::apiResource('accounts', AccountController::class);
    Route::apiResource('outcome_categories', OutcomeCategoriesController::class);
    Route::apiResource('income_operations', IncomeOperationController::class);
    Route::apiResource('outcome_operations', OutcomeOperationController::class);
    Route::apiResource('account_money_transfers', AccountMoneyTransferController::class);
    Route::post('make_actualization', [ActualizationController::class, 'make']);

    Route::group(['prefix' => 'statistics'], function () {
        Route::get('current_balance', [BalanceController::class, 'currentBalance']);
        Route::get('snapshot', [SnapshotReportController::class, 'getReport']);
        Route::get('income', [IncomeCategoriesReportController::class, 'report']);
        Route::get('outcome', [OutcomeCategoriesReportController::class, 'report']);
    });
});
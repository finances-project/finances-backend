<?php

declare(strict_types=1);


namespace App\Http\Resources\Categories;

use App\Models\OutcomeCategory;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin OutcomeCategory */
final class OutcomeCategoryListResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'    => $this->id,
            'title' => $this->title,
        ];
    }
}
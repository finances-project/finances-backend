<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\IncomeOperation;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin IncomeOperation */
final class IncomeOperationResource extends JsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */

    public function toArray($request): array
    {
        return [
            'id'       => $this->id,
            'title'    => $this->title,
            'date'     => $this->date->format('d.m.Y'),
            'sum'      => $this->sum,
            'account'  => $this->account->title,
            'category' => $this->category->title,
            'note'     => $this->note,
        ];
    }
}

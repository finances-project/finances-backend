<?php

declare(strict_types=1);

namespace App\Http\Resources;


use JetBrains\PhpStorm\ArrayShape;

final class SuccessResponse implements \JsonSerializable
{

    #[ArrayShape(['success' => "true"])] public function jsonSerialize(): array
    {
        return [
            'success' => true,
        ];
    }
}
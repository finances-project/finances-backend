<?php

declare(strict_types=1);

namespace App\Http\Requests\OutcomeOperation;

use Illuminate\Foundation\Http\FormRequest;

final class UpdateOutcomeOperationsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title'       => ['required', 'string'],
            'date'        => ['required', 'date_format:Y-m-d'],
            'category_id' => ['required', 'integer'],
            'account_id'  => ['required', 'integer'],
            'sum'         => ['required', 'integer', 'gt:0'],
            'note'        => ['nullable', 'string'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
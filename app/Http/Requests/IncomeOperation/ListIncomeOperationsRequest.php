<?php

declare(strict_types=1);

namespace App\Http\Requests\IncomeOperation;

use App\Components\Paginator;
use Illuminate\Foundation\Http\FormRequest;

final class ListIncomeOperationsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'page'     => ['required', 'integer'],
            'per_page' => ['required', 'integer'],
        ];
    }

    public function getPaginator(): Paginator
    {
        return new Paginator(
            page: (int) $this->input('page'),
            perPage: (int) $this->input('per_page'),
        );
    }

    public function authorize(): bool
    {
        return true;
    }
}
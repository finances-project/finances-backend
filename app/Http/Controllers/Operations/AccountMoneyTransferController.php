<?php

declare(strict_types=1);

namespace App\Http\Controllers\Operations;


use App\UseCases\AccountMoneyTransfer\Add\AddAccountMoneyTransferCommand;
use App\UseCases\AccountMoneyTransfer\Add\AddAccountMoneyTransferCommandHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class AccountMoneyTransferController
{
    public function store(Request $request, AddAccountMoneyTransferCommandHandler $commandHandler): JsonResponse
    {
        $command = new AddAccountMoneyTransferCommand(
            date: new \DateTime($request->date),
            accountIn: (int) $request->account_in,
            accountOut: (int) $request->account_out,
            sum: (int) $request->sum,
            note: $request->note
        );

        $commandHandler->handle($command);

        return new JsonResponse();
    }

}
<?php

declare(strict_types=1);

namespace App\Http\Controllers\Operations;

use App\Http\Controllers\Controller;
use App\Http\Requests\OutcomeOperation\AddOutcomeOperationsRequest;
use App\Http\Requests\OutcomeOperation\ListOutcomeOperationsRequest;
use App\Http\Requests\OutcomeOperation\UpdateOutcomeOperationsRequest;
use App\Http\Resources\IncomeOperationResource;
use App\Http\Resources\SuccessResponse;
use App\Models\OutcomeOperation;
use App\Service\OutcomeOperationService;
use App\UseCases\OutcomingOperation\Add\AddOutgoingOperationCommand;
use App\UseCases\OutcomingOperation\Add\AddOutgoingOperationCommandHandler;
use App\UseCases\OutcomingOperation\Update\UpdateOutgoingOperationCommand;
use App\UseCases\OutcomingOperation\Update\UpdateOutgoingOperationCommandHandler;
use DateTime;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;

final class OutcomeOperationController
{
    private OutcomeOperationService $OutcomeOperationService;

    public function __construct(OutcomeOperationService $operationService)
    {
        $this->OutcomeOperationService = $operationService;
    }

    public function index(ListOutcomeOperationsRequest $request): AnonymousResourceCollection
    {
        return IncomeOperationResource::collection($this->OutcomeOperationService->getAllOperations($request->getPaginator()));
    }


    #[Pure] public function show(OutcomeOperation $operation): IncomeOperationResource
    {
        return new IncomeOperationResource($operation);
    }

    public function store(AddOutcomeOperationsRequest $request, AddOutgoingOperationCommandHandler $handler): SuccessResponse
    {
        $command = new AddOutgoingOperationCommand(
            title: $request->input('title'),
            date: new DateTime($request->input('date')),
            category: (int) $request->input('category_id'),
            account: (int) $request->input('account_id'),
            sum: (int) $request->input('sum'),
            note: $request->input('note')
        );

        $this->OutcomeOperationService->add($command, $handler);

        return new SuccessResponse();
    }


    public function update(UpdateOutcomeOperationsRequest $request, OutcomeOperation $OutcomeOperation, UpdateOutgoingOperationCommandHandler $handler): SuccessResponse
    {
        $command = new UpdateOutgoingOperationCommand(
            id: $OutcomeOperation->id,
            title: $request->input('title'),
            date: new DateTime($request->input('date')),
            category: (int) $request->input('category_id'),
            account: (int) $request->input('account_id'),
            sum: (int) $request->input('sum'),
            note: $request->input('note')
        );

        $this->OutcomeOperationService->update($command, $handler);

        return new SuccessResponse();
    }

    public function destroy(OutcomeOperation $operation): SuccessResponse
    {
        $this->OutcomeOperationService->delete($operation);
        return new SuccessResponse();
    }
}
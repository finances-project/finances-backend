<?php

declare(strict_types=1);

namespace App\Http\Controllers\Operations;

use App\Http\Controllers\Controller;
use App\Http\Requests\IncomeOperation\AddIncomeOperationsRequest;
use App\Http\Requests\IncomeOperation\ListIncomeOperationsRequest;
use App\Http\Requests\IncomeOperation\UpdateIncomeOperationsRequest;
use App\Http\Resources\IncomeOperationResource;
use App\Http\Resources\SuccessResponse;
use App\Models\IncomeOperation;
use App\Service\IncomeOperationService;
use App\UseCases\IncomingOperation\Add\AddIncomingOperationCommand;
use App\UseCases\IncomingOperation\Add\AddIncomingOperationCommandHandler;
use App\UseCases\IncomingOperation\Update\UpdateIncomingOperationCommand;
use App\UseCases\IncomingOperation\Update\UpdateIncomingOperationCommandHandler;
use DateTime;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;

final class IncomeOperationController
{
    private IncomeOperationService $incomeOperationService;

    public function __construct(IncomeOperationService $operationService)
    {
        $this->incomeOperationService = $operationService;
    }

    public function index(ListIncomeOperationsRequest $request): AnonymousResourceCollection
    {
        return IncomeOperationResource::collection($this->incomeOperationService->getAllOperations($request->getPaginator()));
    }


    #[Pure] public function show(IncomeOperation $operation): IncomeOperationResource
    {
        return new IncomeOperationResource($operation);
    }

    public function store(AddIncomeOperationsRequest $request, AddIncomingOperationCommandHandler $handler): SuccessResponse
    {
        $command = new AddIncomingOperationCommand(
            title: $request->input('title'),
            date: new DateTime($request->input('date')),
            category: (int) $request->input('category_id'),
            account: (int) $request->input('account_id'),
            sum: (int) $request->input('sum'),
            note: $request->input('note')
        );

        $this->incomeOperationService->add($command, $handler);

        return new SuccessResponse();
    }


    public function update(UpdateIncomeOperationsRequest $request, IncomeOperation $incomeOperation, UpdateIncomingOperationCommandHandler $handler): SuccessResponse
    {
        $command = new UpdateIncomingOperationCommand(
            id: $incomeOperation->id,
            title: $request->input('title'),
            date: new DateTime($request->input('date')),
            category: (int) $request->input('category_id'),
            account: (int) $request->input('account_id'),
            sum: (int) $request->input('sum'),
            note: $request->input('note')
        );

        $this->incomeOperationService->update($command, $handler);

        return new SuccessResponse();
    }

    public function destroy(IncomeOperation $operation): SuccessResponse
    {
        $this->incomeOperationService->delete($operation);
        return new SuccessResponse();
    }
}
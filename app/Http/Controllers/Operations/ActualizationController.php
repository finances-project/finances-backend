<?php

declare(strict_types=1);

namespace App\Http\Controllers\Operations;


use App\UseCases\Actualization\Make\MakeActualizationCommand;
use App\UseCases\Actualization\Make\MakeActualizationCommandHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class ActualizationController
{
    public function make(Request $request, MakeActualizationCommandHandler $commandHandler): JsonResponse
    {
        $command = new MakeActualizationCommand(
            account: (int) $request->account,
            sum: (int) $request->sum,
            note: $request->note
        );

        $commandHandler->handle($command);

        return new JsonResponse();
    }

}
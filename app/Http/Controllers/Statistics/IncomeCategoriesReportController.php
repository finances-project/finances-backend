<?php

declare(strict_types=1);

namespace App\Http\Controllers\Statistics;


use App\Models\IncomeOperation;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

final class IncomeCategoriesReportController
{
    public function report(Request $request): Collection|array
    {
        $account = (int) $request->account;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $orderBy = $request->order_by ?? 'sum';

        return IncomeOperation::query()
            ->join('income_categories as cat', 'cat.id', '=', 'income_operations.category_id')
            ->whereBetween('income_operations.date',[$startDate,$endDate])
            ->select([
                'cat.title',
                DB::raw('COALESCE(SUM(income_operations.sum),0) as sum'),
                DB::raw('COALESCE(COUNT(income_operations.id),0) as operations'),
            ])
            ->where(['income_operations.account_id' => $account])
            ->groupBy('cat.title')
            ->orderByDesc($orderBy)
            ->get()->each(function ($operation) {
                $operation->sum = number_format((int) $operation->sum, 2, ',', ' ');;
            });
    }
}
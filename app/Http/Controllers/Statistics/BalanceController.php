<?php

declare(strict_types=1);

namespace App\Http\Controllers\Statistics;

use App\Service\BalanceService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class BalanceController
{
    private BalanceService $balanceService;

    public function __construct(BalanceService $balanceService)
    {
        $this->balanceService = $balanceService;
    }

    public function currentBalance(Request $request): JsonResponse
    {
        return new JsonResponse([
            'balance' => $this->balanceService->getCurrentBalance((int) $request->account),
        ]);
    }
}
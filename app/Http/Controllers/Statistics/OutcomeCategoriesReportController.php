<?php

declare(strict_types=1);

namespace App\Http\Controllers\Statistics;


use App\Models\OutcomeOperation;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

final class OutcomeCategoriesReportController
{
    public function report(Request $request): Collection|array
    {
        $account   = (int) $request->account;
        $startDate = $request->start_date;
        $endDate   = $request->end_date;

        $orderBy = $request->order_by ?? 'sum';

        return OutcomeOperation::query()
            ->join('outcome_categories as cat', 'cat.id', '=', 'outcome_operations.category_id')
            ->whereBetween('outcome_operations.date',[$startDate,$endDate])
            ->select([
                'cat.title',
                DB::raw('COALESCE(SUM(outcome_operations.sum),0) as sum'),
                DB::raw('COALESCE(COUNT(outcome_operations.id),0) as operations'),
            ])
            ->where(['outcome_operations.account_id' => $account])
            ->groupBy('cat.title')
            ->orderByDesc($orderBy)
            ->get()->each(function ($operation) {
                $operation->sum = number_format((int) $operation->sum, 2, ',', ' ');;
            });
    }
}
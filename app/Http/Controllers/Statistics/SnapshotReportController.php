<?php

declare(strict_types=1);

namespace App\Http\Controllers\Statistics;


use App\Components\NumberFormatter;
use App\Models\OperationRegister;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

final class SnapshotReportController
{
    public function getReport(Request $request, NumberFormatter $formatter): Collection
    {
        $operationCount = $request->operations ?? 10;
        $account        = $request->account;
        $startDate      = $request->start_date ?? now()->startOfMonth();
        $endDate        = $request->end_date ?? now()->endOfMonth();

        return OperationRegister::query()
            ->orderByDesc('date')
            ->orderByDesc('id')
            ->where(['account_id' => $account])
            ->whereBetween('date', [$startDate, $endDate])
            ->take($operationCount)
            ->with('operation')
            ->get()
            ->map(function (OperationRegister $register) use ($formatter): array {
                return array_merge($register->operation->represent(), [
                    'sum'   => $formatter->formatMoney($register->sum),
                ]);
            });
    }
}
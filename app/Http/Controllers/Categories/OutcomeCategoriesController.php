<?php

declare(strict_types=1);


namespace App\Http\Controllers\Categories;


use App\Http\Resources\Categories\OutcomeCategoryListResource;
use App\Service\OutcomeCategoryService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

final class OutcomeCategoriesController
{

    private OutcomeCategoryService $categoryService;

    public function __construct(OutcomeCategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }


    public function index(): AnonymousResourceCollection
    {
        return OutcomeCategoryListResource::collection($this->categoryService->getCategories());
    }


}
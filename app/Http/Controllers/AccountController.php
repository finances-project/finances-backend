<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\AccountListResource;
use App\Models\Account;
use App\Service\AccountService;
use App\UseCases\Account\Add\AddAccountCommand;
use App\UseCases\Account\Add\AddAccountCommandHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

final class AccountController
{
    private AccountService $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    public function index(): AnonymousResourceCollection
    {
        return AccountListResource::collection($this->accountService->getList());
    }

    public function store(Request $request, AddAccountCommandHandler $handler): JsonResponse
    {
        $command = new AddAccountCommand($request->title);

        $handler->handle($command);

        return new JsonResponse([]);
    }


    public function show(Account $account): Response
    {
        //
    }


    public function update(Request $request, Account $account): Response
    {
        //
    }


    public function destroy(Account $account): Response
    {
        //
    }
}
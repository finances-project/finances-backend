<?php

declare(strict_types=1);

namespace App\Contracts;


interface OperationRepresentation
{
    public function represent(): array;
}
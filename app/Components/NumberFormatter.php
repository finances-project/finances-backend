<?php

declare(strict_types=1);

namespace App\Components;


final class NumberFormatter
{
    public function formatMoney(int $value): string
    {
        return number_format($value, 2, ',', ' ');
    }
}
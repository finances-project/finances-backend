<?php

declare(strict_types=1);

namespace App\Components;


use Webmozart\Assert\Assert;

final class Paginator
{

    public int $page;
    public int $perPage;

    /**
     * Paginator constructor.
     * @param  int  $page
     * @param  int  $perPage
     */
    public function __construct(int $page, int $perPage)
    {
        Assert::positiveInteger($page);
        Assert::positiveInteger($perPage);
        $this->page    = $page;
        $this->perPage = $perPage;
    }
}
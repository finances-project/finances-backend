<?php

declare(strict_types=1);

namespace App\Service;


use App\Components\Paginator;
use App\Models\IncomeOperation;
use App\UseCases\IncomingOperation\Add\AddIncomingOperationCommand;
use App\UseCases\IncomingOperation\Add\AddIncomingOperationCommandHandler;
use App\UseCases\IncomingOperation\Update\UpdateIncomingOperationCommand;
use App\UseCases\IncomingOperation\Update\UpdateIncomingOperationCommandHandler;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\ConnectionInterface;

final class IncomeOperationService
{
    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function getAllOperations(Paginator $paginator): LengthAwarePaginator
    {
        return IncomeOperation::query()
            ->with(['account:id,title', 'category:id,title'])
            ->orderByDesc('date')
            ->paginate($paginator->perPage);
    }

    public function add(AddIncomingOperationCommand $command, AddIncomingOperationCommandHandler $handler)
    {
        return $this->connection->transaction(function () use ($command, $handler): IncomeOperation {
            $operation = $handler->handle($command);
            $operation->createOperationRegisters();

            return $operation;
        });
    }

    public function update(UpdateIncomingOperationCommand $command, UpdateIncomingOperationCommandHandler $handler)
    {
        return $this->connection->transaction(function () use ($command, $handler): IncomeOperation {
            $operation = $handler->handle($command);
            $operation->operation_registers()->delete();
            $operation->createOperationRegisters();

            return $operation;
        });
    }

    public function delete(IncomeOperation $operation): bool
    {
        return $this->connection->transaction(function () use ($operation): bool {
            $operation->operation_registers()->delete();
            $operation->delete();

            return true;
        });
    }
}
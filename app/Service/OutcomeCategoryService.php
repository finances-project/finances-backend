<?php


namespace App\Service;


use App\Models\OutcomeCategory;
use Illuminate\Database\Eloquent\Collection;

class OutcomeCategoryService
{
    public function getCategories(): Collection
    {
        return OutcomeCategory::query()->get();
    }
}
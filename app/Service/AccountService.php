<?php

declare(strict_types=1);

namespace App\Service;


use App\Models\Account;
use Illuminate\Database\Eloquent\Collection;

final class AccountService
{
    public function getList(): Collection|array
    {
        return Account::query()->get();
    }
}
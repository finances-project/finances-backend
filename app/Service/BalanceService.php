<?php

declare(strict_types=1);

namespace App\Service;


use Illuminate\Database\ConnectionInterface;

final class BalanceService
{
    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function getCurrentBalance(int $account): int
    {
        return (int) $this->connection
            ->table('operation_registers')
            ->where(['account_id' => $account])
            ->sum('sum');
    }
}
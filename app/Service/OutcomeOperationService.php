<?php

declare(strict_types=1);

namespace App\Service;


use App\Components\Paginator;
use App\Models\OutcomeOperation;
use App\UseCases\OutcomingOperation\Add\AddOutgoingOperationCommand;
use App\UseCases\OutcomingOperation\Add\AddOutgoingOperationCommandHandler;
use App\UseCases\OutcomingOperation\Update\UpdateOutgoingOperationCommand;
use App\UseCases\OutcomingOperation\Update\UpdateOutgoingOperationCommandHandler;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\ConnectionInterface;

final class OutcomeOperationService
{
    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function getAllOperations(Paginator $paginator): LengthAwarePaginator
    {
        return OutcomeOperation::query()
            ->with(['account:id,title', 'category:id,title'])
            ->orderByDesc('date')
            ->paginate($paginator->perPage);
    }

    public function add(AddOutgoingOperationCommand $command, AddOutgoingOperationCommandHandler $handler)
    {
        return $this->connection->transaction(function () use ($command, $handler): OutcomeOperation {
            $operation = $handler->handle($command);
            $operation->createOperationRegisters();

            return $operation;
        });
    }

    public function update(UpdateOutgoingOperationCommand $command, UpdateOutgoingOperationCommandHandler $handler)
    {
        return $this->connection->transaction(function () use ($command, $handler): OutcomeOperation {
            $operation = $handler->handle($command);
            $operation->operation_registers()->delete();
            $operation->createOperationRegisters();

            return $operation;
        });
    }

    public function delete(OutcomeOperation $operation): bool
    {
        return $this->connection->transaction(function () use ($operation): bool {
            $operation->operation_registers()->delete();
            $operation->delete();

            return true;
        });
    }
}
<?php

declare(strict_types=1);

namespace App\UseCases\Actualization\Make;


final class MakeActualizationCommand
{

    public int $account;
    public int $sum;
    public ?string $note;

    public function __construct(int $account, int $sum,  ?string $note = null)
    {
        $this->account        = $account;
        $this->sum            = $sum;
        $this->note           = $note;
    }
}
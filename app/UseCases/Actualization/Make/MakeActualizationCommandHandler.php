<?php

declare(strict_types=1);

namespace App\UseCases\Actualization\Make;


use App\Models\Actualization;
use App\Service\BalanceService;
use Illuminate\Database\ConnectionInterface;
use RuntimeException;

final class MakeActualizationCommandHandler
{

    private ConnectionInterface $connection;
    private BalanceService $balanceService;

    public function __construct(ConnectionInterface $connection, BalanceService $balanceService)
    {
        $this->connection     = $connection;
        $this->balanceService = $balanceService;
    }

    public function handle(MakeActualizationCommand $command): Actualization
    {
        return $this->connection->transaction(function () use ($command): Actualization {
            $balance = $this->balanceService->getCurrentBalance($command->account);

            if ($balance === $command->sum) {
                throw new RuntimeException(sprintf('Баланс уже корректен : %d', $balance));
            }

            $actualization = new Actualization();

            $actualization->date       = new \DateTime();
            $actualization->account_id = $command->account;
            $actualization->sum        = $command->sum - $balance;
            $actualization->note       = $command->note;

            $actualization->save();

            $actualization->createOperationRegisters();

            return $actualization;
        });
    }
}
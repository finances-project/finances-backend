<?php

declare(strict_types=1);

namespace App\UseCases\IncomingOperation\Add;


use App\Models\IncomeOperation;

final class AddIncomingOperationCommandHandler
{
    public function handle(AddIncomingOperationCommand $command): IncomeOperation
    {
        $operation              = new IncomeOperation();
        $operation->title       = $command->title;
        $operation->date        = $command->date;
        $operation->category_id = $command->category;
        $operation->account_id  = $command->account;
        $operation->sum         = $command->sum;
        $operation->note        = $command->note;

        $operation->save();

        return $operation;
    }
}
<?php

declare(strict_types=1);

namespace App\UseCases\IncomingOperation\Update;


final class UpdateIncomingOperationCommand
{
    public string $title;
    public \DateTime $date;
    public int $category;
    public int $account;
    public int $sum;
    public ?string $note;
    public int $id;

    public function __construct(int $id, string $title, \DateTime $date, int $category, int $account, int $sum, ?string $note = null)
    {
        $this->id = $id;
        $this->title    = $title;
        $this->date     = $date;
        $this->category = $category;
        $this->account  = $account;
        $this->sum      = $sum;
        $this->note     = $note;

    }
}
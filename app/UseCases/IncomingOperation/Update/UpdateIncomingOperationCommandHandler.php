<?php

declare(strict_types=1);

namespace App\UseCases\IncomingOperation\Update;


use App\Models\IncomeOperation;

final class UpdateIncomingOperationCommandHandler
{
    public function handle(UpdateIncomingOperationCommand $command): IncomeOperation
    {
        $operation              = IncomeOperation::query()->find($command->id);
        $operation->title       = $command->title;
        $operation->date        = $command->date;
        $operation->category_id = $command->category;
        $operation->account_id  = $command->account;
        $operation->sum         = $command->sum;
        $operation->note        = $command->note;

        $operation->update();

        return $operation;
    }
}
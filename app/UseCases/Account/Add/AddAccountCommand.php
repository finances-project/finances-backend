<?php


namespace App\UseCases\Account\Add;


class AddAccountCommand
{
    public function __construct(public string $title) {}

}
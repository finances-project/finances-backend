<?php


namespace App\UseCases\Account\Add;


use App\Models\Account;

class AddAccountCommandHandler
{
    public function handle(AddAccountCommand $command): Account
    {
        return Account::query()->create([
            'title' => $command->title,
        ]);
    }
}
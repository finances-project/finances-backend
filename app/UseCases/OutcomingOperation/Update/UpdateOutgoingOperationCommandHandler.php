<?php

declare(strict_types=1);

namespace App\UseCases\OutcomingOperation\Update;


use App\Models\OutcomeOperation;

final class UpdateOutgoingOperationCommandHandler
{
    public function handle(UpdateOutgoingOperationCommand $command): OutcomeOperation
    {
        $operation              = OutcomeOperation::query()->find($command->id);
        $operation->title       = $command->title;
        $operation->date        = $command->date;
        $operation->category_id = $command->category;
        $operation->account_id  = $command->account;
        $operation->sum         = $command->sum;
        $operation->note        = $command->note;

        $operation->update();

        return $operation;
    }
}
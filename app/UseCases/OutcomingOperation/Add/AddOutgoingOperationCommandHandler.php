<?php

declare(strict_types=1);

namespace App\UseCases\OutcomingOperation\Add;


use App\Models\OutcomeOperation;

final class AddOutgoingOperationCommandHandler
{
    public function handle(AddOutgoingOperationCommand $command): OutcomeOperation
    {
        $operation              = new OutcomeOperation();
        $operation->title       = $command->title;
        $operation->date        = $command->date;
        $operation->category_id = $command->category;
        $operation->account_id  = $command->account;
        $operation->sum         = $command->sum;
        $operation->note        = $command->note;

        $operation->save();

        return $operation;
    }
}
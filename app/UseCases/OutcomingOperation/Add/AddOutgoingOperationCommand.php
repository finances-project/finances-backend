<?php

declare(strict_types=1);

namespace App\UseCases\OutcomingOperation\Add;


final class AddOutgoingOperationCommand
{
    public string $title;
    public \DateTime $date;
    public int $category;
    public int $account;
    public int $sum;
    public ?string $note;

    public function __construct(string $title, \DateTime $date, int $category, int $account, int $sum, ?string $note = null)
    {
        $this->title    = $title;
        $this->date     = $date;
        $this->category = $category;
        $this->account  = $account;
        $this->sum      = $sum;
        $this->note     = $note;
    }
}
<?php

declare(strict_types=1);

namespace App\UseCases\AccountMoneyTransfer\Add;


final class AddAccountMoneyTransferCommand
{
    public \DateTime $date;
    public int $accountIn;
    public int $accountOut;
    public int $sum;
    public ?string $note;

    public function __construct(\DateTime $date, int $accountIn, int $accountOut, int $sum, ?string $note = null)
    {
        $this->date       = $date;
        $this->accountIn  = $accountIn;
        $this->accountOut = $accountOut;
        $this->sum        = $sum;
        $this->note       = $note;
    }
}
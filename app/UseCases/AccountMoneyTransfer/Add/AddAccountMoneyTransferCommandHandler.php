<?php

declare(strict_types=1);

namespace App\UseCases\AccountMoneyTransfer\Add;


use App\Models\AccountMoneyTransfer;
use Illuminate\Database\ConnectionInterface;

final class AddAccountMoneyTransferCommandHandler
{

    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function handle(AddAccountMoneyTransferCommand $command): AccountMoneyTransfer
    {
        return $this->connection->transaction(function () use ($command): AccountMoneyTransfer {
            if ($command->accountOut === $command->accountIn) {
                throw new \RuntimeException('Нельзя осуществлять перевод внутри одного счёта');
            }

            $transfer                 = new AccountMoneyTransfer();
            $transfer->date           = $command->date;
            $transfer->account_in_id  = $command->accountIn;
            $transfer->account_out_id = $command->accountOut;
            $transfer->sum            = $command->sum;
            $transfer->note           = $command->note;

            $transfer->save();

            $transfer->createOperationRegisters();

            return $transfer;
        });
    }
}
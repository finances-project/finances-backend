<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IncomeCategory
 *
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\IncomeOperation[] $operations
 * @property-read int|null $operations_count
 * @method static \Database\Factories\IncomeCategoryFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeCategory whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class IncomeCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
    ];


    public function operations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(IncomeOperation::class, 'category_id');
    }
}
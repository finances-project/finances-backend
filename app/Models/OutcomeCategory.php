<?php

declare(strict_types=1);

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\OutcomeCategory
 *
 * @property int $id
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OutcomeOperation[] $operations
 * @property-read int|null $operations_count
 * @method static \Database\Factories\OutcomeCategoryFactory factory(...$parameters)
 * @method static Builder|OutcomeCategory newModelQuery()
 * @method static Builder|OutcomeCategory newQuery()
 * @method static Builder|OutcomeCategory query()
 * @method static Builder|OutcomeCategory whereCreatedAt($value)
 * @method static Builder|OutcomeCategory whereId($value)
 * @method static Builder|OutcomeCategory whereTitle($value)
 * @method static Builder|OutcomeCategory whereUpdatedAt($value)
 * @mixin Eloquent
 */
final class OutcomeCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
    ];


    public function operations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OutcomeOperation::class,'category_id');
    }
}
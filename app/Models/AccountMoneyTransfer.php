<?php

declare(strict_types=1);

namespace App\Models;

use App\Contracts\OperationRepresentation;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Ramsey\Uuid\Uuid;

/**
 * App\Models\AccountMoneyTransfer
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $date
 * @property int $account_in_id
 * @property int $account_out_id
 * @property int $sum
 * @property string|null $note
 * @property-read \App\Models\Account $account_in
 * @property-read \App\Models\Account $account_out
 * @property-read Collection|\App\Models\OperationRegister[] $operation_registers
 * @property-read int|null $operation_registers_count
 * @method static Builder|AccountMoneyTransfer newModelQuery()
 * @method static Builder|AccountMoneyTransfer newQuery()
 * @method static Builder|AccountMoneyTransfer query()
 * @method static Builder|AccountMoneyTransfer whereAccountInId($value)
 * @method static Builder|AccountMoneyTransfer whereAccountOutId($value)
 * @method static Builder|AccountMoneyTransfer whereDate($value)
 * @method static Builder|AccountMoneyTransfer whereId($value)
 * @method static Builder|AccountMoneyTransfer whereNote($value)
 * @method static Builder|AccountMoneyTransfer whereSum($value)
 * @mixin Eloquent
 */
final class AccountMoneyTransfer extends Model implements OperationRepresentation
{
    public $timestamps = false;

    protected $fillable = [
        'date',
        'account_in_id',
        'account_out_id',
        'sum',
        'note',
    ];

    protected $dates = [
        'date',
    ];

    public function account_in(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }

    public function account_out(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }


    public function operation_registers(): MorphMany
    {
        return $this->morphMany(OperationRegister::class, 'operation');
    }

    public function represent(): array
    {
        return [
            'title' => 'Перевод между счетами',
            'date'  => $this->date->format('d.m.Y'),
            'note'  => $this->note,
            'uuid' => Uuid::uuid4()
        ];
    }

    public function createOperationRegisters(): void
    {
        $this->operation_registers()->createMany([
            [
                'date'       => $this->date,
                'account_id' => $this->account_out_id,
                'sum'        => -$this->sum,
            ],
            [
                'date'       => $this->date,
                'account_id' => $this->account_in_id,
                'sum'        => $this->sum,
            ],
        ]);
    }
}
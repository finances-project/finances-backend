<?php

declare(strict_types=1);

namespace App\Models;

use App\Contracts\OperationRepresentation;
use Database\Factories\OutcomeOperationFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * App\Models\OutcomeOperation
 *
 * @property int $id
 * @property string $title
 * @property Carbon $date
 * @property int $category_id
 * @property int $account_id
 * @property int $sum
 * @property string|null $note
 * @property-read Account $account
 * @property-read IncomeCategory $category
 * @property-read Collection|OperationRegister[] $operation_registers
 * @property-read int|null $operation_registers_count
 * @method static OutcomeOperationFactory factory(...$parameters)
 * @method static Builder|OutcomeOperation newModelQuery()
 * @method static Builder|OutcomeOperation newQuery()
 * @method static Builder|OutcomeOperation query()
 * @method static Builder|OutcomeOperation whereAccountId($value)
 * @method static Builder|OutcomeOperation whereCategoryId($value)
 * @method static Builder|OutcomeOperation whereDate($value)
 * @method static Builder|OutcomeOperation whereId($value)
 * @method static Builder|OutcomeOperation whereNote($value)
 * @method static Builder|OutcomeOperation whereSum($value)
 * @method static Builder|OutcomeOperation whereTitle($value)
 * @mixin Eloquent
 */
final class OutcomeOperation extends Model implements OperationRepresentation
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'date',
        'category_id',
        'account_id',
        'sum',
        'note',
    ];

    protected $dates = [
        'date',
    ];


    public function operation_registers(): MorphMany
    {
        return $this->morphMany(OperationRegister::class, 'operation');
    }

    public function createOperationRegisters(): void
    {
        $this->operation_registers()->create([
            'date'       => $this->date,
            'account_id' => $this->account_id,
            'sum'        => -$this->sum,
        ]);
    }

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(IncomeCategory::class, 'category_id');
    }

    public function represent(): array
    {
        return [
            'title' => $this->title,
            'date'  => $this->date->format('d.m.Y'),
            'note'  => $this->note,
            'uuid' => Uuid::uuid4()
        ];    }
}
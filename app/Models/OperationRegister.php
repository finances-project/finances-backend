<?php

declare(strict_types=1);

namespace App\Models;

use App\Contracts\OperationRepresentation;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;


/**
 * App\Models\OperationRegister
 *
 * @property int $id
 * @property Carbon $date
 * @property string $operation_type
 * @property int $operation_id
 * @property int $account_id
 * @property int $sum
 * @property-read Account $account
 * @property-read Model|Eloquent|OperationRepresentation $operation
 * @method static Builder|OperationRegister newModelQuery()
 * @method static Builder|OperationRegister newQuery()
 * @method static Builder|OperationRegister query()
 * @method static Builder|OperationRegister whereAccountId($value)
 * @method static Builder|OperationRegister whereDate($value)
 * @method static Builder|OperationRegister whereId($value)
 * @method static Builder|OperationRegister whereOperationId($value)
 * @method static Builder|OperationRegister whereOperationType($value)
 * @method static Builder|OperationRegister whereSum($value)
 * @mixin Eloquent
 */
final class OperationRegister extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'date',
        'operation_id',
        'account_id',
        'sum'
    ];

    protected $dates = [
        'date'
    ];

    public function operation(): MorphTo
    {
        return $this->morphTo();
    }

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }
}
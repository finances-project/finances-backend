<?php

declare(strict_types=1);

namespace App\Models;

use App\Contracts\OperationRepresentation;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * App\Models\Actualization
 *
 * @property int $id
 * @property Carbon $date
 * @property int $account_id
 * @property int $sum
 * @property string|null $note
 * @property-read Account $account
 * @property-read Collection|OperationRegister[] $operation_registers
 * @property-read int|null $operation_registers_count
 * @method static Builder|Actualization newModelQuery()
 * @method static Builder|Actualization newQuery()
 * @method static Builder|Actualization query()
 * @method static Builder|Actualization whereAccountId($value)
 * @method static Builder|Actualization whereDate($value)
 * @method static Builder|Actualization whereId($value)
 * @method static Builder|Actualization whereNote($value)
 * @method static Builder|Actualization whereSum($value)
 * @mixin Eloquent
 */
final class Actualization extends Model implements OperationRepresentation
{
    public $timestamps = false;

    protected $fillable = [
        'date',
        'account_id',
        'sum',
        'note',
    ];

    protected $dates = ['date'];

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }

    public function operation_registers(): MorphMany
    {
        return $this->morphMany(OperationRegister::class, 'operation');
    }

    public function createOperationRegisters(): void
    {
        $this->operation_registers()->create([
            'date'       => $this->date,
            'account_id' => $this->account_id,
            'sum'        => $this->sum,
        ]);
    }

    public function represent(): array
    {
        return [
            'title' => 'Актуализация счёта',
            'date'  => $this->date->format('d.m.Y'),
            'note'  => $this->note,
            'uuid' => Uuid::uuid4()
        ];
    }
}
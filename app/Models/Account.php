<?php

declare(strict_types=1);

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Account
 *
 * @property int $id
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Account newModelQuery()
 * @method static Builder|Account newQuery()
 * @method static Builder|Account query()
 * @method static Builder|Account whereCreatedAt($value)
 * @method static Builder|Account whereId($value)
 * @method static Builder|Account whereTitle($value)
 * @method static Builder|Account whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\IncomeOperation[] $income_operations
 * @property-read int|null $income_operations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OutcomeOperation[] $outcome_operations
 * @property-read int|null $outcome_operations_count
 * @method static \Database\Factories\AccountFactory factory(...$parameters)
 */
final class Account extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
    ];

    public function income_operations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(IncomeOperation::class);
    }

    public function outcome_operations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OutcomeOperation::class);
    }
}